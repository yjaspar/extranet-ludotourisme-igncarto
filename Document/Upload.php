<?php
namespace Extranet\LudotourismeIGNCartoBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Upload
{
	/**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /** @MongoDB\Field(type="file") */
    private $file;

    /** @MongoDB\Field(type="string") */
    private $fileName;

    /** @MongoDB\Field(type="string") */
    private $mimeType;

    /** @MongoDB\Field(type="date") */
    private $updated;

    /** @MongoDB\Field(type="date") */
    private $created;

    /** @MongoDB\Field(type="int") */
    private $length;

    /** @MongoDB\Field(type="int") */
    private $chunkSize;

    /** @MongoDB\Field(type="string") */
    private $md5;

    public function getId(){
        return ($this->id);
    }

    public function getFile(){
        return ($this->file);
    }

    public function setFile($value){
        $this->file=$value;
    }

    public function getFileName(){
        return ($this->fileName);
    }

    public function setFileName($value){
        $this->fileName=$value;
    }

    public function getMimeType(){
        return ($this->mimeType);
    }

    public function setMimeType($value){
        $this->mimeType=$value;
    }

    public function getLength(){
        return ($this->length);
    }

    public function setLength($value){
        $this->length=$value;
    }

    public function getChunkSize(){
        return ($this->chunkSize);
    }

    public function setChunkSize($value){
        $this->chunkSize=$value;
    }

    public function getMD5(){
        return ($this->md5);
    }

    public function setMD5($value){
        $this->md5=$value;
    }

    public function getUpdated(){
        return ($this->updated);
    }

    public function setUpdated(){
        $this->updated=date('M-d-Y');
    }

    public function getCreated(){
        return ($this->created);
    }

    public function setcreated(){
        $this->created=date('M-d-Y');
    }
}
?>