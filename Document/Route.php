<?php
namespace Extranet\LudotourismeIGNCartoBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(
 *     db="Extranet_ceesto",
 *     collection="LudotourismeIGN___Route"
 * )
 */
class Route
{
	/**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Extranet\DashboardBundle\Document\User")
     */
    protected $author;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Extranet\LudotourismeIGNCartoBundle\Document\RouteType")
     */
    private $routeType;

    /** @MongoDB\Field(type="int") */
    private $department;

    /** @MongoDB\Field(type="int") */
    private $area;

    /** @MongoDB\Field(type="boolean") */
    private $isActivated;

    /** @MongoDB\Field(type="boolean") */
    private $isAvailable;

    /** @MongoDB\Field(type="boolean") */
    private $isTreasure;

    /** @MongoDB\Field(type="string") */
    private $reference;

    /** @MongoDB\Field(type="int") */
    private $displayZone;

    /** @MongoDB\Field(type="hash") */
    private $languages;

    /** @MongoDB\Field(type="hash") */
    private $steps;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Extranet\DashboardBundle\Document\Upload")
     */
    private $attachmentFr;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Extranet\DashboardBundle\Document\Upload")
     */
    private $attachmentEn;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Extranet\DashboardBundle\Document\Upload")
     */
    private $gpxWaypoint;
    
    /**
     * @MongoDB\ReferenceOne(targetDocument="Extranet\DashboardBundle\Document\Upload")
     */
    private $gpxRando;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Extranet\DashboardBundle\Document\Upload")
     */
    private $gpxDisplay;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Extranet\DashboardBundle\Document\Upload")
     */
    private $image;

    /** @MongoDB\Field(type="date") */
    private $updated;

    /** @MongoDB\Field(type="date") */
    private $created;

    public function getId(){
        return ($this->id);
    }

    public function getAuthor(){
        return ($this->author);
    }

    public function setAuthor($value){
        $this->author=$value;
    }

    public function getRouteType(){
        return ($this->routeType);
    }

    public function setRouteType($value){
        $this->routeType=$value;
    }

    public function getArea(){
        return ($this->area);
    }

    public function setArea($value){
        $this->area=$value;
    }

    public function getDepartment(){
        return ($this->department);
    }

    public function setDepartment($value){
        $this->department=$value;
    }

    public function getIsActivated(){
        return ($this->isActivated);
    }

    public function setIsActivated($value){
        $this->isActivated=$value;
    }

    public function getIsAvailable(){
        return ($this->isAvailable);
    }

    public function setIsAvailable($value){
        $this->isAvailable=$value;
    }

    public function getIsTreasure(){
        return ($this->isTreasure);
    }

    public function setIsTreasure($value){
        $this->isTreasure=$value;
    }

    public function getReference(){
        return ($this->reference);
    }

    public function setReference($value){
        $this->reference=$value;
    }

    public function getDisplayZone(){
        return ($this->displayZone);
    }

    public function setDisplayZone($value){
        $this->displayZone=$value;
    }

    public function getLanguages($language=null){
        
        return($language?$this->languages[$language]:$this->languages);
    }

    public function getAttachmentFr(){
        return($this->attachmentFr);
    }

    public function getAttachmentEn(){
        return($this->attachmentEn);
    }

    public function setAttachmentFr($value){
        $this->attachmentFr = $value;
    }

    public function setAttachmentEn($value){
        $this->attachmentEn = $value;
    }

    public function setLanguages($languages){
        $this->languages=$languages;
    }

    public function getSteps($step=null){
        return($step?$this->steps[$step]:$this->steps);
    }

    public function setSteps($steps){
        $this->steps=$steps;
    }

    public function getGPXWaypoint(){
        return ($this->gpxWaypoint);
    }

    public function setGPXWaypoint($value){
        $this->gpxWaypoint=$value;
    }

    public function getGPXRando(){
        return ($this->gpxRando);
    }

    public function setGPXRando($value){
        $this->gpxRando=$value;
    }

    public function getGPXDisplay(){
        return ($this->gpxDisplay);
    }

    public function setGPXDisplay($value){
        $this->gpxDisplay=$value;
    }

    public function getImage(){
        return ($this->image);
    }

    public function setImage($value){
        $this->image=$value;
    }
}
?>