<?php
namespace Extranet\LudotourismeIGNCartoBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(
 *     db="Extranet_ceesto",
 *     collection="LudotourismeIGN___RouteType"
 * )
 */
class RouteType
{
	/**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /** @MongoDB\Field(type="string") */
    protected $name;

    
    /**
     * @MongoDB\ReferenceOne(targetDocument="Extranet\DashboardBundle\Document\Upload")
     */
    protected $image;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $idRT;

	/**
     * @MongoDB\Field(strategy="string")
     */
    protected $color;

	/**
     * @MongoDB\Field(strategy="date")
     */
    protected $updated;

	/**
     * @MongoDB\Field(strategy="date")
     */
    protected $created;

	public function getId(){
        return ($this->id);
    }

    public function setIdRT($value){
        $this->idRT = $value;
    }

    public function getIdRT(){
        return ($this->idRT);
    }

    public function getName(){
        return ($this->name);
    }

    public function setName($value){
        $this->name=$value;
    }

    public function getImage(){
        return ($this->image);
    }

    public function setImage($value){
        $this->image=$value;
    }

    public function getColor(){
        return ($this->color);
    }

    public function setColor($value){
        $this->color=$value;
    }
}

?>