<?php

namespace Extranet\LudotourismeIGNCartoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\SecurityContext;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/hello/{name}")
     * @Template()
     */
    public function indexAction()
    {
		$request = Request::createFromGlobals();

	    $response = new Response();
	    $response->headers->set('Content-Type', 'application/json');

		$dm = $this->get('doctrine_mongodb')->getManager();
		if ($request->isXmlHttpRequest()){
			if ($this->get('security.context')->isGranted('ROLE_ADMIN') ||
				$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')){
				$response->setContent('{value:"1"}');
			}else{
				$response->setContent('{value:""}');
			}
			return $response;
		} else {
			if ($this->get('security.context')->isGranted('ROLE_ADMIN') ||
				$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')){
				if (!isset($_GET['route_type']))
					$maps = $dm->getRepository('ExtranetLudotourismeIGNCartoBundle:Route')->findAll();
				else
					$maps = $dm->getRepository('ExtranetLudotourismeIGNCartoBundle:Route')->findBy(array('routeType.$id' => new \MongoId($_GET['route_type'])));
			}else{
				$dm = $this->get('doctrine_mongodb')->getManager();
				if (!isset($_GET['route_type'])){
					$qb = $dm->createQueryBuilder('ExtranetLudotourismeIGNCartoBundle:Route');
			        $qb->field('author.$id')->equals(new \MongoId($this->get('security.context')->getToken()->getUser()->getId()));
			        $maps = $qb->getQuery()->execute();
			    }else{
			    	$qb = $dm->createQueryBuilder('ExtranetLudotourismeIGNCartoBundle:Route');
			        $qb->field('author.$id')->equals(new \MongoId($this->get('security.context')->getToken()->getUser()->getId()));
			        $qb->field('routeType.$id')->equals(new \MongoId($_GET['route_type']));
			        $maps = $qb->getQuery()->execute();
			    }
			}

			$types = $dm->getRepository('ExtranetLudotourismeIGNCartoBundle:RouteType')->findAll();

		    return $this->render('ExtranetLudotourismeIGNCartoBundle:Default:index.html.twig', array('self' => $this->get('security.context')->getToken()->getUser(),'admin' => ($this->get('security.context')->isGranted('ROLE_ADMIN') ||
		    			$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')), 'sadmin' => $this->get('security.context')->isGranted('ROLE_SUPER_ADMIN'), 'title' => 'Ludotourisme IGN', 'maps' => $maps, 'types' => $types, 'selector' => isset($_GET['route_type'])?$_GET['route_type']:''));
		}
    }

	public function deleteRouteTypeAction($id){
		if ($this->get('security.context')->isGranted('ROLE_ADMIN') ||
			$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')){
			$dm = $this->get('doctrine_mongodb')->getManager();
			
			$type = $dm->getRepository('ExtranetLudotourismeIGNCartoBundle:RouteType')
			            ->find($id);
	        if ($type->getImage()){
	        	$image = $dm->getRepository('ExtranetLudotourismeIGNCartoBundle:Upload')
			    	        ->find($type->getImage()->getId());
				$dm->remove($image);
			}
			$dm->remove($type);

			$dm->flush();
		}
		return $this->redirect($this->generateUrl('LudoIGN'));
	}

	public function deleteRouteAction($id){
		$dm = $this->get('doctrine_mongodb')->getManager();

		if ($this->get('security.context')->isGranted('ROLE_ADMIN') ||
			$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN'))
			$route = $dm->getRepository('ExtranetLudotourismeIGNCartoBundle:Route')->findOneBy(array(
				'_id' => new \MongoId($id)
			));
		else
			$route = $dm->getRepository('ExtranetLudotourismeIGNCartoBundle:Route')->findOneBy(array(
				'_id' => new \MongoId($id),
				'author.$id' => new \MongoId($this->get('security.context')->getToken()->getUser()->getId())
			));

        
        if ($route->getAttachmentFr())
			$dm->remove($route->getAttachmentFr());
		if ($route->getAttachmentEn())
			$dm->remove($route->getAttachmentEn());
		if ($route->getGPXDisplay())
			$dm->remove($route->getGPXDisplay());
		if ($route->getGPXRando())
			$dm->remove($route->getGPXRando());
		if ($route->getGPXWaypoint())
			$dm->remove($route->getGPXWaypoint());
		if ($route->getImage())
			$dm->remove($route->getImage());
		
		$dm->remove($route);

		$dm->flush();

		return $this->redirect($this->generateUrl('LudoIGN'));	
	}

	public function editRouteTypeAction($id){
		if ($this->get('security.context')->isGranted('ROLE_ADMIN') ||
			$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')){
			$dm = $this->get('doctrine_mongodb')->getManager();
			$type = $dm->getRepository('ExtranetLudotourismeIGNCartoBundle:RouteType')
			            ->find($id);
			if ($_POST){
				if (($_FILES['image']['tmp_name']!='' &&  isset($_POST['old_image']) && $_POST['old_image'] != '' )
				|| (isset($_POST['delete_old_image']) && $_POST['delete_old_image'] == 'on')){
					$img = $dm->getRepository('ExtranetLudotourismeIGNCartoBundle:Upload')
							->find($_POST['old_image']);
					$dm->createQueryBuilder('ExtranetLudotourismeIGNCartoBundle:RouteType')
					    ->update()
					    ->field('image')->unsetField()->exists(true)
					    ->getQuery()
					    ->execute();
					$dm->remove($img);
				}
				if ($_FILES['image']['tmp_name']!=''){
					$size = getimagesize($_FILES['image']['tmp_name']);
				    
					$upload = new \Extranet\DashboardBundle\Document\Upload();
					$upload->setHeight($size[1]);
					$upload->setWidth($size[0]);
					$upload->setFile($_FILES['image']['tmp_name']);
					$upload->setFileName($_FILES['image']['name']);
					$upload->setMimeType($_FILES['image']['type']);
					$upload->setLength($_FILES['image']['size']);

					$dm->persist($upload);
					$dm->flush();
					$type->setImage($upload);
				}
				$type->setName($_POST['name']);
				$type->setIdRT($_POST['id']);
				$type->setColor($_POST['color']);
				$dm->flush();
				return $this->redirect($this->generateUrl('LudoIGN'));
			}
		}
		return $this->render('ExtranetLudotourismeIGNCartoBundle:Default:editRT.html.twig', array('self' => $this->get('security.context')->getToken()->getUser(), 'title' => 'Ludotourisme IGN', 'type' => $type));
	    
    }

    public function addRouteTypeAction(){
		if ($this->get('security.context')->isGranted('ROLE_ADMIN') ||
			$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')){
			$dm = $this->get('doctrine_mongodb')->getManager();
			
			$type = new \Extranet\LudotourismeIGNCartoBundle\Document\RouteType();

			if (isset($_FILES['image']) && $_FILES['image']['tmp_name']!=''){
				$size = getimagesize($_FILES['image']['tmp_name']);

				$upload = new \Extranet\DashboardBundle\Document\Upload();
				$upload->setFile($_FILES['image']['tmp_name']);
				$upload->setHeight($size[1]);
				$upload->setWidth($size[0]);
				$upload->setFileName($_FILES['image']['name']);
				$upload->setMimeType($_FILES['image']['type']);
				$upload->setLength($_FILES['image']['size']);
			    
			    $dm->persist($upload);
			    $dm->flush();
			    $type->setImage($upload);
			}
			
		    $type->setName($_POST['name']);
		    $type->setIdRT($_POST['id']);
		    $type->setColor($_POST['color']);

			$dm->persist($type);
		    $dm->flush();
		}
	    return $this->redirect($this->generateUrl('LudoIGN'));
    }

    public function editRouteAction($id){
    	$dm = $this->get('doctrine_mongodb')->getManager();

    	$route = $dm->getRepository('ExtranetLudotourismeIGNCartoBundle:Route')
		            ->findOneById($id);
		if ($_POST){
			
			if ($this->get('security.context')->isGranted('ROLE_ADMIN') ||
			$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')){
				$author = $dm->getRepository('ExtranetDashboardBundle:User')
			            ->findOneByEmail($_POST['author']);
				$route->setAuthor($author);
			}
			
			$route->setRouteType($dm->getRepository('ExtranetLudotourismeIGNCartoBundle:RouteType')->findOneById($_POST['routeType']));
			$route->setDepartment($_POST['department']);
			$route->setReference($_POST['reference']);
			$route->setDisplayZone($_POST['displayZone']);
			$route->setIsAvailable((isset($_POST['isAvailable'])&&$_POST['isAvailable']=='on')?true:false);
			
			if ($this->get('security.context')->isGranted('ROLE_ADMIN') || $this->get('security.context')->isGranted('ROLE_SUPER_ADMIN'))
				$route->setIsActivated(isset($_POST['isActivated'])?true:false);
			else
				$route->setIsActivated(false);
			
			$route->setIsTreasure((isset($_POST['isTreasure'])&&$_POST['isTreasure']=='on')?true:false);
			
			
			if (($_FILES['en_attachment']['tmp_name']!='' &&  isset($_POST['en_attachment_old']) && $_POST['en_attachment_old'] != '' )
			|| (isset($_POST['delete_old_en_attachment']) && $_POST['delete_old_en_attachment'] == 'on')){
				$img = $dm->getRepository('ExtranetLudotourismeIGNCartoBundle:Upload')
						->findOneById($_POST['en_attachment_old']);
				$dm->remove($img);
				
				$route->setAttachmentEn(null);
			}

			if (($_FILES['fr_attachment']['tmp_name']!='' &&  isset($_POST['fr_attachment_old']) && $_POST['fr_attachment_old'] != '' )
			|| (isset($_POST['delete_old_fr_attachment']) && $_POST['delete_old_fr_attachment'] == 'on')){
				$img = $dm->getRepository('ExtranetLudotourismeIGNCartoBundle:Upload')
						->find($_POST['fr_attachment_old']);
				$dm->remove($img);

				$route->setAttachmentFr(null);
			}

			$route->setLanguages(array('fr' => array(
					'name' => $_POST['fr_name'],
					'title' => $_POST['fr_title'],
					'body' => $_POST['fr_body'],
					'attachment' => null
				), 'en' => array(
					'name' => $_POST['en_name'],
					'title' => $_POST['en_title'],
					'body' => $_POST['en_body'],
					'attachment' => null
			)));

			if ($_FILES['fr_attachment']['tmp_name']!=''){
				$fr_attachment = new \Extranet\DashboardBundle\Document\Upload();
				$fr_attachment->setFile($_FILES['fr_attachment']['tmp_name']);
				$fr_attachment->setFileName($_FILES['fr_attachment']['name']);
				$fr_attachment->setMimeType($_FILES['fr_attachment']['type']);
				$fr_attachment->setLength($_FILES['fr_attachment']['size']);
			    
			    $dm->persist($fr_attachment);
			    $dm->flush();

			    $route->setAttachmentFr($fr_attachment);
			}

			if ($_FILES['en_attachment']['tmp_name']!=''){				
				$en_attachment = new \Extranet\DashboardBundle\Document\Upload();
				$en_attachment->setFile($_FILES['en_attachment']['tmp_name']);
				$en_attachment->setFileName($_FILES['en_attachment']['name']);
				$en_attachment->setMimeType($_FILES['en_attachment']['type']);
				$en_attachment->setLength($_FILES['en_attachment']['size']);
			    
			    $dm->persist($en_attachment);
			    $dm->flush();

			    $route->setAttachmentEn($en_attachment);
			}


			if (($_FILES['GPXRando']['tmp_name']!='' &&  isset($_POST['GPXRando_old']) && $_POST['GPXRando_old'] != '' )
			|| (isset($_POST['delete_old_GPXRando']) && $_POST['delete_old_GPXRando'] == 'on')){
				$img = $dm->getRepository('ExtranetLudotourismeIGNCartoBundle:Upload')->find($_POST['GPXRando_old']);
				$dm->remove($img);

				$route->setGPXRando(null);
			}

			if ($_FILES['GPXRando']['tmp_name']!=''){
				$gpxRando = new \Extranet\DashboardBundle\Document\Upload();
				$gpxRando->setFile($_FILES['GPXRando']['tmp_name']);
				$gpxRando->setFileName($_FILES['GPXRando']['name']);
				$gpxRando->setMimeType($_FILES['GPXRando']['type']);
				$gpxRando->setLength($_FILES['GPXRando']['size']);
			    
			    $dm->persist($gpxRando);
			    $dm->flush();
				
				$route->setGPXRando($gpxRando);
			}


			if (($_FILES['GPXDisplay']['tmp_name']!='' &&  isset($_POST['GPXDisplay_old']) && $_POST['GPXDisplay_old'] != '' )
			|| (isset($_POST['delete_old_GPXDisplay']) && $_POST['delete_old_GPXDisplay'] == 'on')){
				$img = $dm->getRepository('ExtranetLudotourismeIGNCartoBundle:Upload')
						->find($_POST['GPXDisplay_old']);
				$dm->remove($img);

				$route->setGPXDisplay(null);
			}

			if ($_FILES['GPXDisplay']['tmp_name']!=''){
			    $gpxDisplay = new \Extranet\DashboardBundle\Document\Upload();
				$gpxDisplay->setFile($_FILES['GPXDisplay']['tmp_name']);
				$gpxDisplay->setFileName($_FILES['GPXDisplay']['name']);
				$gpxDisplay->setMimeType($_FILES['GPXDisplay']['type']);
				$gpxDisplay->setLength($_FILES['GPXDisplay']['size']);
			    
			    $dm->persist($gpxDisplay);
			    $dm->flush();
				
				$route->setGPXDisplay($gpxDisplay);
			}


			if (($_FILES['GPXWaypoint']['tmp_name']!='' &&  isset($_POST['GPXWaypoint_old']) && $_POST['GPXWaypoint_old'] != '' )
			|| (isset($_POST['delete_old_GPXWaypoint']) && $_POST['delete_old_GPXWaypoint'] == 'on')){
				$img = $dm->getRepository('ExtranetLudotourismeIGNCartoBundle:Upload')
						->find($_POST['GPXWaypoint_old']);
				$dm->remove($img);

				$route->setGPXWaypoint(null);
			}

			if ($_FILES['GPXWaypoint']['tmp_name']!=''){
			    $gpxWaypoint = new \Extranet\DashboardBundle\Document\Upload();
				$gpxWaypoint->setFile($_FILES['GPXWaypoint']['tmp_name']);
				$gpxWaypoint->setFileName($_FILES['GPXWaypoint']['name']);
				$gpxWaypoint->setMimeType($_FILES['GPXWaypoint']['type']);
				$gpxWaypoint->setLength($_FILES['GPXWaypoint']['size']);
			    
			    $dm->persist($gpxWaypoint);
			    $dm->flush();

			    $route->setGPXWaypoint($gpxWaypoint);
			}


			if (($_FILES['image']['tmp_name']!='' &&  isset($_POST['image_old']) && $_POST['image_old'] != '' )
			|| (isset($_POST['delete_old_image']) && $_POST['delete_old_image'] == 'on')){
				$img = $dm->getRepository('ExtranetLudotourismeIGNCartoBundle:Upload')
						->find($_POST['image_old']);
				$dm->remove($img);

				$route->setImage(null);
			}

			if ($_FILES['image']['tmp_name']!=''){
				$size = getimagesize($_FILES['image']['tmp_name']);

			    $image = new \Extranet\DashboardBundle\Document\Upload();
			    $image->setHeight($size[1]);
				$image->setWidth($size[0]);
				$image->setFile($_FILES['image']['tmp_name']);
				$image->setFileName($_FILES['image']['name']);
				$image->setMimeType($_FILES['image']['type']);
				$image->setLength($_FILES['image']['size']);
			    
			    $dm->persist($image);
			    $dm->flush();

			    $route->setImage($image);
			}

			$route->setSteps(array(
				'degree_x' => $_POST['deg_x'],
				'degree_y' => $_POST['deg_y'],
				'minute_x' => $_POST['min_x'],
				'minute_y' => $_POST['min_y'],
				'second_x' => $_POST['sec_x'],
				'second_y' => $_POST['sec_y'],
				'direction_x' => $_POST['dir_x'],
				'direction_y' => $_POST['dir_y'],
				'coord_x' => $this->convertDMStoLatLong($_POST['deg_x'], $_POST['min_x'], $_POST['sec_x']),
				'coord_y' => $this->convertDMStoLatLong($_POST['deg_y'], $_POST['min_y'], $_POST['sec_y']),
				'type' => $_POST['type']
			));

			$dm->persist($route);
		    $dm->flush();

			return $this->redirect($this->generateUrl('LudoIGN'));
		}
		$types = $dm->getRepository('ExtranetLudotourismeIGNCartoBundle:RouteType')
		            ->findAll();
    	return $this->render('ExtranetLudotourismeIGNCartoBundle:Default:editR.html.twig', array('self' => $this->get('security.context')->getToken()->getUser(), 'title'=> 'Edit Route', 'map' => $route, 'types' => $types, 'admin' => ($this->get('security.context')->isGranted('ROLE_ADMIN') ||
	    			$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN'))));
    }

    public function addRouteAction(){
		$dm = $this->get('doctrine_mongodb')->getManager();

		$author = $dm->getRepository('ExtranetDashboardBundle:User')
		            ->findOneByEmail($_POST['author']);

		$route = new \Extranet\LudotourismeIGNCartoBundle\Document\Route();

		$route->setAuthor($author);
		
		$route->setRouteType($dm->getRepository('ExtranetLudotourismeIGNCartoBundle:RouteType')->findOneById($_POST['routeType']));
		$route->setDepartment($_POST['department']);
		$route->setReference($_POST['reference']);
		$route->setDisplayZone($_POST['displayZone']);
		$route->setIsAvailable($_POST['isAvailable']);

		if ($this->get('security.context')->isGranted('ROLE_ADMIN') || $this->get('security.context')->isGranted('ROLE_SUPER_ADMIN'))
			$route->setIsActivated($_POST['isActivated']);
		else
			$route->setIsActivated(false);

		$route->setIsTreasure($_POST['isTreasure']);

		$route->setLanguages(array('fr' => array(
				'name' => $_POST['fr_name'],
				'title' => $_POST['fr_title'],
				'body' => $_POST['fr_body'],
				'attachment' => null
			), 'en' => array(
				'name' => $_POST['en_name'],
				'title' => $_POST['en_title'],
				'body' => $_POST['en_body'],
				'attachment' => null
		)));

		if ($_FILES['fr_attachment']['tmp_name']!=''){
			$fr_attachment = new \Extranet\DashboardBundle\Document\Upload();
			$fr_attachment->setFile($_FILES['fr_attachment']['tmp_name']);
			$fr_attachment->setFileName($_FILES['fr_attachment']['name']);
			$fr_attachment->setMimeType($_FILES['fr_attachment']['type']);
			$fr_attachment->setLength($_FILES['fr_attachment']['size']);
		    
		    $dm->persist($fr_attachment);
		    $dm->flush();

		    $route->setAttachmentFr($fr_attachment);
		}

		if ($_FILES['en_attachment']['tmp_name']!=''){
			$en_attachment = new \Extranet\DashboardBundle\Document\Upload();
			$en_attachment->setFile($_FILES['en_attachment']['tmp_name']);
			$en_attachment->setFileName($_FILES['en_attachment']['name']);
			$en_attachment->setMimeType($_FILES['en_attachment']['type']);
			$en_attachment->setLength($_FILES['en_attachment']['size']);
		    
		    $dm->persist($en_attachment);
		    $dm->flush();

		    $route->setAttachmentEn($en_attachment);
		}

		if ($_FILES['GPXRando']['tmp_name']!=''){
			$gpxRando = new \Extranet\DashboardBundle\Document\Upload();
			$gpxRando->setFile($_FILES['GPXRando']['tmp_name']);
			$gpxRando->setFileName($_FILES['GPXRando']['name']);
			$gpxRando->setMimeType($_FILES['GPXRando']['type']);
			$gpxRando->setLength($_FILES['GPXRando']['size']);
		    
		    $dm->persist($gpxRando);
		    $dm->flush();
			
			$route->setGPXRando($gpxRando);
		}

		if ($_FILES['GPXDisplay']['tmp_name']!=''){
		    $gpxDisplay = new \Extranet\DashboardBundle\Document\Upload();
			$gpxDisplay->setFile($_FILES['GPXDisplay']['tmp_name']);
			$gpxDisplay->setFileName($_FILES['GPXDisplay']['name']);
			$gpxDisplay->setMimeType($_FILES['GPXDisplay']['type']);
			$gpxDisplay->setLength($_FILES['GPXDisplay']['size']);
		    
		    $dm->persist($gpxDisplay);
		    $dm->flush();
			
			$route->setGPXDisplay($gpxDisplay);
		}

		if ($_FILES['GPXWaypoint']['tmp_name']!=''){
		    $gpxWaypoint = new \Extranet\DashboardBundle\Document\Upload();
			$gpxWaypoint->setFile($_FILES['GPXWaypoint']['tmp_name']);
			$gpxWaypoint->setFileName($_FILES['GPXWaypoint']['name']);
			$gpxWaypoint->setMimeType($_FILES['GPXWaypoint']['type']);
			$gpxWaypoint->setLength($_FILES['GPXWaypoint']['size']);
		    
		    $dm->persist($gpxWaypoint);
		    $dm->flush();

		    $route->setGPXWaypoint($gpxWaypoint);
		}

		if ($_FILES['image']['tmp_name']!=''){
		    $size = getimagesize($_FILES['image']['tmp_name']);

		    $image = new \Extranet\DashboardBundle\Document\Upload();
			$image->setFile($_FILES['image']['tmp_name']);
			$image->setFileName($_FILES['image']['name']);
			$image->setHeight($size[1]);
			$image->setWidth($size[0]);
			$image->setMimeType($_FILES['image']['type']);
			$image->setLength($_FILES['image']['size']);
		    
		    $dm->persist($image);
		    $dm->flush();

		    $route->setImage($image);
		}

		$route->setSteps(array(
			'degree_x' => $_POST['deg_x'],
			'degree_y' => $_POST['deg_y'],
			'minute_x' => $_POST['min_x'],
			'minute_y' => $_POST['min_y'],
			'second_x' => $_POST['sec_x'],
			'second_y' => $_POST['sec_y'],
			'direction_x' => $_POST['dir_x'],
			'direction_y' => $_POST['dir_y'],
			'coord_x' => $this->convertDMStoLatLong($_POST['deg_x'], $_POST['min_x'], $_POST['sec_x']),
			'coord_y' => $this->convertDMStoLatLong($_POST['deg_y'], $_POST['min_y'], $_POST['sec_y']),
			'type' => $_POST['type']
		));

		$dm->persist($route);
	    $dm->flush();

	    return $this->redirect($this->generateUrl('LudoIGN'));
    }

    public function carteAction($center_lat = null, $center_long = null, $zoom = null, $transparency = null, $height = null, $width = null, $menu = null, $areas = null, $layers = null){

        $dm = $this->get('doctrine_mongodb')->getManager();
        $transparency=((float)$transparency/100);
        
        $layers = explode(',', $layers);

        $areas = explode('-', $areas);
		$qb = $dm->createQueryBuilder('ExtranetLudotourismeIGNCartoBundle:Route');
		$qb->field('isActivated')->equals(true);
		$qb->field('isAvailable')->equals(true);
        
        $res = null;
	        foreach($areas as $key => $value){
	        	$var=explode(',', $value);
	        	
	        	if (sizeof($var) > 1){
		        	if (sizeof($areas) > 1){ // Plantage sizeof($var) > 2
		        		for ($i=1; $i<sizeof($var); $i++){
			    			
			    			$qb->addOr(
			    				$qb->expr()->field('department')->equals((int)$var[0])->field('displayZone')->equals((int)$var[$i])
			    			);
			    		}
		        	}elseif(sizeof($var) > 2){
						for ($i=1; $i<sizeof($var); $i++){
		    				$qb->field('department')->equals((int)$var[0])->addOr(
		    					$qb->expr()->field('displayZone')->equals((int)$var[$i])
		    				);
			    		}
		        	}else{
		        		$qb->field('department')->equals((int)$var[0])->field('displayZone')->equals((int)$var[1]);
		        	}
	        	}else{
	        		if (sizeof($areas) > 1) // Multiple department, addOr management
	        			$qb->addOr($qb->expr()->field('department')->equals((int)$var[0]));
	        		else // One department, no need addMore
	        			$qb->field('department')->equals((int)$var[0]);
	        	}
	        }
			$res = $qb->getQuery()->execute();

        $center_long = explode(',', $center_long);
        $center_lat = explode(',', $center_lat);

        $center[1]=$this->convertDMStoLatLong($center_long[0], $center_long[1], $center_long[2]);
        $center[0]=$this->convertDMStoLatLong($center_lat[0], $center_lat[1], $center_lat[2]);
        
        $zoom = $zoom;
        
        $maps = array(0 => array('name' => 'Tous', 'routes' => ''));
        foreach ($res as $value) {
        	foreach($layers as $layer){
        		if ($layer == $value->getRouteType()->getIdRT()){
					if (!isset($maps[$value->getRouteType()->getId()]))
		                    $maps[$value->getRouteType()->getId()] = array('name' => $value->getRouteType()->getName(), 'routes' => (string) $value->getId());
		            else
		                $maps[$value->getRouteType()->getId()]['routes'] .= "," . $value->getId();
		        	$maps[0]['routes'].=($maps[0]['routes']==""?(string) $value->getId():','.(string) $value->getId());
		        }
        	}
        }

        return $this->render('ExtranetLudotourismeIGNCartoBundle:Default:carte.html.twig', array('self' => $this->get('security.context')->getToken()->getUser(),
        					'title'=> '', 'center_x' => $center[0], 'center_y' => $center[1], 'zoom' => (int) $zoom, 'transparency' => $transparency, 'height' => $height,
        					'width' => $width, 'menu' => $menu, 'areas' => $areas, 'layers' => $layers, 'routes' => $maps));
	}

    public function geokmlAction($id=null) {
        $steps=null;
        if ($id){
	        $ids = explode(',', $id);

			$dm = $this->get('doctrine_mongodb')->getManager();
			$qb = $dm->createQueryBuilder('ExtranetLudotourismeIGNCartoBundle:Route');

	        foreach($ids as $id)
				$qb->addOr($qb->expr()->field('_id')->equals(new \MongoId($id)));
			$steps = $qb->getQuery();
		}

        return $this->render('ExtranetLudotourismeIGNCartoBundle:Default:geokml.kml.twig', array('steps' => $steps));
    }

    private function convertDMStoLatLong($deg, $min, $sec) {
        // Converts DMS ( Degrees / minutes / seconds ) 
        // to decimal format longitude / latitude

        return $deg + ((($min * 60) + ($sec)) / 3600);
    }

    private function convertLatLongtoDMS($dec) {
        // Converts decimal longitude / latitude to DMS
        // ( Degrees / minutes / seconds ) 
        // This is the piece of code which may appear to 
        // be inefficient, but to avoid issues with floating
        // point math we extract the integer part and the float
        // part by using a string function.

        $vars = explode(".", $dec);
        $deg = $vars[0];
        $tempma = "0." . $vars[1];

        $tempma = $tempma * 3600;
        $min = floor($tempma / 60);
        $sec = $tempma - ($min * 60);

        return array("deg" => $deg, "min" => $min, "sec" => $sec);
    }
}
